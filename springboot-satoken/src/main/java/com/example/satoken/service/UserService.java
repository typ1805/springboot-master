package com.example.satoken.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.satoken.domain.User;

/**
 * @ClassName： UserService.java
 * @ClassPath： com.example.satoken.service.UserService.java
 * @Description： 用户信息
 * @Author： tanyp
 * @Date： 2023/3/16 10:06
 **/
public interface UserService extends IService<User> {
}
