package com.example.satoken.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.satoken.domain.User;
import com.example.satoken.mapper.UserMapper;
import com.example.satoken.service.UserService;
import org.springframework.stereotype.Service;

/**
 * @ClassName： UserServiceImpl.java
 * @ClassPath： com.example.satoken.service.impl.UserServiceImpl.java
 * @Description： 用户信息
 * @Author： tanyp
 * @Date： 2023/3/16 10:07
 **/
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
}
