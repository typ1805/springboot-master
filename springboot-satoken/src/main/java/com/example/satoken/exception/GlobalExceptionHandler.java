package com.example.satoken.exception;

import cn.dev33.satoken.util.SaResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @ClassName： GlobalExceptionHandler.java
 * @ClassPath： com.example.satoken.exception.GlobalExceptionHandler.java
 * @Description： 全局异常拦截
 * @Author： tanyp
 * @Date： 2023/3/16 10:15
 **/
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler
    public SaResult handlerException(Exception e) {
        e.printStackTrace();
        return SaResult.error(e.getMessage());
    }

}
