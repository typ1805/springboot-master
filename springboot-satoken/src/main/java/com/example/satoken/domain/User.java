package com.example.satoken.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @ClassName： User.java
 * @ClassPath： com.example.satoken.domain.User.java
 * @Description： 用户信息
 * @Author： tanyp
 * @Date： 2023/3/16 10:02
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "user")
public class User {

    // 主键ID
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    // 账号
    private String username;

    // 密码
    private String password;

    // 创建时间
    private LocalDateTime createTime;

}
