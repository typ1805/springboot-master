package com.example.satoken.controller;

import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.example.satoken.domain.User;
import com.example.satoken.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

/**
 * @ClassName： UserController.java
 * @ClassPath： com.example.satoken.controller.UserController.java
 * @Description： 用户信息
 * @Author： tanyp
 * @Date： 2023/3/16 10:08
 **/
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("login")
    public Object login(String username, String password) {
        User user = userService.getOne(Wrappers.<User>lambdaQuery().ge(User::getUsername, username));
        if (Objects.nonNull(user) && Objects.equals(user.getUsername(), username) && Objects.equals(user.getPassword(), password)) {
            // 登录鉴权
            StpUtil.login(user.getId());
            // 返回token信息
            return StpUtil.getTokenInfo();
        }
        return "登录失败，用户名或密码有误！";
    }

    @GetMapping("logout")
    public Object logout() {
        StpUtil.logout();
        return "登出成功!";
    }

}
