package com.example.satoken.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaIgnore;
import cn.dev33.satoken.stp.StpUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName： TestController.java
 * @ClassPath： com.example.satoken.controller.TestController.java
 * @Description： 注解鉴权
 * @Author： tanyp
 * @Date： 2023/3/16 10:16
 **/
@SaCheckLogin // 登录校验 —— 只有登录之后才能进入该方法。
@RestController
@RequestMapping("/test")
public class TestController {

    /**
     * @MonthName： test
     * @Description： 忽略校验 —— 表示被修饰的方法或类无需进行注解鉴权和路由拦截器鉴权。
     * @Author： tanyp
     * @Date： 2023/3/16 10:17
     * @Param： []
     * @return： java.lang.String
     **/
    @SaIgnore // 忽略校验
    @GetMapping("test")
    public String test() {
        return "当前会话是否登录：" + StpUtil.isLogin();
    }

}
