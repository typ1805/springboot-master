package com.example.satoken.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.satoken.domain.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClassName： UserMapper.java
 * @ClassPath： com.example.satoken.mapper.UserMapper.java
 * @Description： 用户信息
 * @Author： tanyp
 * @Date： 2023/3/16 10:06
 **/
@Mapper
public interface UserMapper extends BaseMapper<User> {
}
