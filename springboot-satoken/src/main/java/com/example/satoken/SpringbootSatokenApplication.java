package com.example.satoken;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootSatokenApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootSatokenApplication.class, args);
    }

}
