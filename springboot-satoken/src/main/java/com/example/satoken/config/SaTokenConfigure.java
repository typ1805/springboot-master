package com.example.satoken.config;

import cn.dev33.satoken.interceptor.SaInterceptor;
import cn.dev33.satoken.router.SaRouter;
import cn.dev33.satoken.stp.StpUtil;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @ClassName： SaTokenConfigure.java
 * @ClassPath： com.example.satoken.config.SaTokenConfigure.java
 * @Description： 鉴权拦截器
 * @Author： tanyp
 * @Date： 2023/3/16 10:14
 **/
@Configuration
public class SaTokenConfigure implements WebMvcConfigurer {

    /**
     * @MonthName： addInterceptors
     * @Description： 注册 Sa-Token 拦截器，打开注解式鉴权功能
     * @Author： tanyp
     * @Date： 2023/3/16 10:19
     * @Param： [registry]
     * @return： void
     **/
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 注册 Sa-Token 拦截器，打开注解式鉴权功能
        registry.addInterceptor(new SaInterceptor(handle -> StpUtil.checkLogin()))
                // 验证所有接口
                .addPathPatterns("/**")
                // 忽略校验
                .excludePathPatterns("/user/login");

        /*
        // 根据路由划分模块，不同模块不同鉴权
        registry.addInterceptor(new SaInterceptor(handler -> {
            SaRouter.match("/user/**", r -> StpUtil.checkPermission("user"));
            SaRouter.match("/admin/**", r -> StpUtil.checkPermission("admin"));
            SaRouter.match("/goods/**", r -> StpUtil.checkPermission("goods"));
            SaRouter.match("/orders/**", r -> StpUtil.checkPermission("orders"));
            SaRouter.match("/notice/**", r -> StpUtil.checkPermission("notice"));
            // 更多模块...
        })).addPathPatterns("/**");*/
    }

}
